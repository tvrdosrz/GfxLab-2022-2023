package xyz.marsavic.gfxlab.graphics3d.scene;


import xyz.marsavic.functions.interfaces.F1;
import xyz.marsavic.geometry.Vector;
import xyz.marsavic.gfxlab.SplineSpectrum;
import xyz.marsavic.gfxlab.Vec3;
import xyz.marsavic.gfxlab.graphics3d.*;
import xyz.marsavic.gfxlab.graphics3d.solids.Ball;
import xyz.marsavic.gfxlab.graphics3d.solids.Group;
import xyz.marsavic.gfxlab.graphics3d.solids.HalfSpace;
import xyz.marsavic.gfxlab.graphics3d.solids.Parallelepiped;
import xyz.marsavic.gfxlab.graphics3d.textures.Grid;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class SpectrumTest extends Scene.Base {

    public SpectrumTest() {
        var materialUVWalls  = (F1<Material, Vector>) (uv -> Material.matte(1.0));
        var materialBlocks = (F1<Material, Vector>) (uv -> Material.matte(0.0));
        var materialUVWallsL = Grid.standard(w -> 1.0);
        var materialUVWallsB = Grid.standard(w -> 1.0);
        var materialCoverBlock = Grid.standard(w -> 0.0);

        var materialUVWallsR = Grid.standard(w -> 1.0);

        var materialGlass = (F1<Material, Vector>) (uv -> Material.GLASS
                .refractiveIndex(w -> 5.6 + (w-400)/(800-400) * (1.55 - 5.6)));
        var materialMirror = (F1<Material, Vector>) (uv -> Material.MIRROR);

        var materialLight = (F1<Material, Vector>) (uv -> Material.light(w -> 1.0));

        Collection<Solid> solids = new ArrayList<>();
        Collections.addAll(solids,
                HalfSpace.pn(Vec3.xyz(-1,  0,  0), Vec3.xyz( 1,  0,  0), materialUVWallsL),
                HalfSpace.pn(Vec3.xyz( 1,  0,  0), Vec3.xyz(-1,  0,  0), materialUVWallsR),
                HalfSpace.pn(Vec3.xyz( 0, -1,  0), Vec3.xyz( 0,  1,  0), materialUVWalls),
//                HalfSpace.pn(Vec3.xyz( 0,  1,  0), Vec3.xyz( 0, -1,  0), materialUVWalls),
                HalfSpace.pn(Vec3.xyz( 0,  0,  1), Vec3.xyz( 0,  0, -1), materialUVWallsB),
                HalfSpace.pn(Vec3.xyz( 0,  0, -6), Vec3.xyz( 0,  0,  1), materialUVWallsB),
                Ball.cr(Vec3.xyz(0, 6, 0), 0.8, materialLight),
                Parallelepiped.pabc(Vec3.xyz(-0.25, 0, 0.25),
                        Vec3.xyz(0.6, 0.8, 0),
                        Vec3.xyz(-0.2, -0.6, 0),
                        Vec3.xyz(0, 0, -0.5),
                        materialGlass),

                Parallelepiped.pabc(Vec3.xyz(-1, 1, 1),
                        Vec3.xyz(0.75, 0, 0),
                        Vec3.xyz(0, 0.4, 0),
                        Vec3.xyz(0, 0, -6),
                        materialCoverBlock),
                Parallelepiped.pabc(Vec3.xyz(0.25, 1, 1),
                        Vec3.xyz(2, 0, 0),
                        Vec3.xyz(0, 0.4, 0),
                        Vec3.xyz(0, 0, -6),
                        materialCoverBlock),
                Parallelepiped.pabc(Vec3.xyz(-0.25, 1, 1),
                        Vec3.xyz(0.5, 0, 0),
                        Vec3.xyz(0, 0.4, 0),
                        Vec3.xyz(0, 0, -0.75),
                        materialCoverBlock),
                Parallelepiped.pabc(Vec3.xyz(-0.25, 1, -0.25),
                        Vec3.xyz(0.5, 0, 0),
                        Vec3.xyz(0, 0.4, 0),
                        Vec3.xyz(0, 0, -6),
                        materialCoverBlock)
        );

        solid = Group.of(solids);
    }
}
