package xyz.marsavic.gfxlab.graphics3d;

import javafx.util.Pair;
import xyz.marsavic.gfxlab.Color;
import xyz.marsavic.gfxlab.Spectrum;
import xyz.marsavic.gfxlab.SplineSpectrum;

public record Material(
		Spectrum diffuse,
		Spectrum specular,
		Spectrum emissive,
		double shininess,
		Spectrum reflective,
		Spectrum refractive,
		Spectrum refractiveIndex

) {
	public Material diffuse        (Spectrum diffuse        ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material specular       (Spectrum  specular      ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material emissive       (Spectrum emissive       ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material shininess      (double shininess        ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material reflective     (Spectrum  reflective    ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material refractive     (Spectrum  refractive    ) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	public Material refractiveIndex(Spectrum refractiveIndex) { return new Material(diffuse, specular, emissive, shininess, reflective, refractive, refractiveIndex); }
	// Since refractive index is a function from wavelength to a real number, it can be viewed as a spectrum

	public static final Material BLACK   = new Material(w -> 0, w -> 0, w -> 0, 32, w -> 0, w -> 0, w -> 1.5);
	
	public static Material matte (Spectrum s) { return BLACK.diffuse(s); }
	public static Material matte (double k) { return matte(w -> k); }
	public static Material matte (        ) { return matte(w -> 1.0); } // TODO: potentially have to replace with D65
	public static final Material MATTE = matte();
	
	public static final Material MIRROR = BLACK.reflective(new SplineSpectrum(new Pair[]{
		new Pair<Double, Double>(248.0, 0.926),
		new Pair<Double, Double>(400.0, 0.920),
		new Pair<Double, Double>(532.0, 0.916),
		new Pair<Double, Double>(633.0, 0.907),
		new Pair<Double, Double>(800.0, 0.868)
	}));
	public static final Material GLASS = BLACK.refractive(w -> 1.0)
			.refractiveIndex(w -> 1.6 + (w-400)/(800-400) * (1.55 - 1.6)); /* Made to roughly resemble refractive index
			                                                                  of BaK4 crown glass*/

	public static Material light (Spectrum s) { return BLACK.emissive(s); }
}
