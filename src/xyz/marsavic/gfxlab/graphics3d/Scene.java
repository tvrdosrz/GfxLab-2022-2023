package xyz.marsavic.gfxlab.graphics3d;

import xyz.marsavic.gfxlab.Color;
import xyz.marsavic.gfxlab.Spectrum;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public interface Scene {
	Solid solid();
	
	Collection<Light> lights();
	
	Spectrum backgroundSpectrum = wavelength -> 0;


	class Base implements Scene {
		
		protected Solid solid;
		protected final List<Light> lights = new ArrayList<>();
		
		@Override
		public Solid solid() {
			return solid;
		}
		
		@Override
		public Collection<Light> lights() {
			return lights;
		}
		
		public final Spectrum backgroundSpectrum = wavelength -> 0;
		
	}
	
	
}
