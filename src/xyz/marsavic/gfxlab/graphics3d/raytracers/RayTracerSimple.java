package xyz.marsavic.gfxlab.graphics3d.raytracers;

import xyz.marsavic.geometry.Vec;
import xyz.marsavic.gfxlab.Color;
import xyz.marsavic.gfxlab.Vec3;
import xyz.marsavic.gfxlab.graphics3d.*;
import xyz.marsavic.random.RNG;
import xyz.marsavic.utils.Numeric;

public class RayTracerSimple extends RayTracer {

	private static final int spectrumSamples = 20;
	private static final double minWavelength = 380;
	private static final double maxWavelength = 780;
	private static final double EPSILON = 1e-9;
	private static final int sampleNumber = 4;
	private static final double stopProbability = 1.0;

	public RayTracerSimple(Scene scene, Camera camera) {
		super(scene, camera);
	}

	public static RNG rng = new RNG();

	@Override
	protected Color sample(Ray ray) {
		Color result = Color.BLACK;
		for (int i = 0; i < sampleNumber; i++) {
			double x = 0.0, y = 0.0, z = 0.0;
			for (int j = 0; j < spectrumSamples; j++) {
				double wavelength = minWavelength + (((double) j + rng.nextDouble()) / spectrumSamples) * (maxWavelength - minWavelength);
				double intensity = sample(ray, wavelength, 7);

				x += intensity * Xyz.x[(int) wavelength];
				y += intensity * Xyz.y[(int) wavelength];
				z += intensity * Xyz.z[(int) wavelength];
			}

			result = result.add(Color.xyz(x, y, z));
		}
		return result.div(sampleNumber);
	}

	protected double sample(Ray ray, double wavelength, int depthRemaining) {
		double returnFactor = 1.0;
		if (depthRemaining <= 0) {
			if (rng.nextDouble(1.0) <= stopProbability) {
				return 0.0;
			} else {
				returnFactor = 1/(1-stopProbability);
			}
		}
		
		Hit hit = scene.solid().firstHit(ray, EPSILON);
		if (hit == null) {
			return scene.backgroundSpectrum.at(wavelength);
		}

		Vec3 p = ray.at(hit.t());                 // The hit point
		Vec3 n_ = hit.n_();                       // Normalized normal to the body surface at the hit point
		Vec3 i = ray.d().inverse();               // Incoming direction
		double lI = i.length();
		Vec3 r = GeometryUtils.reflectedN(n_, i); // Reflected ray (i reflected over n)
		Vec3 r_ = r.div(lI);                      // Reflected ray (i reflected over n)

		Material material = hit.material();

		double lightDiffuse  = 0.0;        // The diffuse contribution of the generated path
		double lightSpecular = 0.0;        // The specular contribution of the generated path
		double lightReflected = 0.0;      // The reflective contribution of the generated path
		double lightRefracted = 0.0;      // The refractive contribution of the generated path
		double lightEmissive = material.emissive().at(wavelength); // The contribution of the light surface itself

		double result = lightEmissive;

		double diffuse = material.diffuse().at(wavelength);
		if (diffuse != 0.0) {
			double r1 = rng.nextDouble(1.0); // Angle of projected random vector in the plain normal to n_
			double r2 = rng.nextDouble(0.25); // Angle of vector compared to n_
			Vec3 u_ = GeometryUtils.normal(n_).normalized_(); // One of the normalized vectors normal to n
			Vec3 v_ = n_.cross(u_); // Doesn't need to be normalized because n_ and u_ are normalized and normal
			Vec3 o = u_.mul(Numeric.sinT(r1)).add(v_.mul(Numeric.cosT(r1)))
					.mul(Numeric.cosT(r2)).add(n_.mul(Numeric.sinT(r2))); // Outgoing sample vector
			lightDiffuse = diffuse * sample(Ray.pd(p, o), wavelength, depthRemaining - 1);
		}

		result += lightDiffuse;
		double reflective = material.reflective().at(wavelength);
		if (reflective != 0.0) {
			lightReflected = sample(Ray.pd(p, r), wavelength, depthRemaining - 1);
			result += reflective * lightReflected;
		}

		double refractive = material.refractive().at(wavelength);
		if (refractive != 0.0) {
			Vec3 b; // refracted light vector
			double rInd = 1/material.refractiveIndex().at(wavelength);

			double iCosN = i.dot(n_);
			if (iCosN < 0) {
				rInd = 1/rInd;
			}
			Vec3 iProjection = n_.mul(iCosN);
			Vec3 iRejection = i.sub(iProjection);
			double iSinSqr = iRejection.lengthSquared()/i.lengthSquared();
			double bSinSqr = iSinSqr*rInd*rInd;
			if (bSinSqr > 1) {
				b = r;
			} else {
				Vec3 bRejection = iRejection.inverse();
				Vec3 bProjection = n_.mul(Math.sqrt(bRejection.lengthSquared()*(1-bSinSqr)/bSinSqr));
				if (iCosN > 0) {
					bProjection = bProjection.inverse();
				}
				b = bRejection.add(bProjection);
			}
			lightRefracted = sample(Ray.pd(p, b), wavelength, depthRemaining - 1);
			result += refractive * lightRefracted;
		}

		return returnFactor * result;
	}
}
