package xyz.marsavic.gfxlab.graphics3d;

import xyz.marsavic.gfxlab.Spectrum;
import xyz.marsavic.gfxlab.Vec3;

/** Point light. */
public record Light(
		Vec3 p,
		Spectrum s
) {
	
	public static Light ps(Vec3 p, Spectrum s) {
		return new Light(p, s);
	}
	
	public static Light p(Vec3 p) {
		return ps(p, wavelength -> 1.0);
	}
	
}
