package xyz.marsavic.gfxlab;

import javafx.util.Pair;
import java.util.Arrays;
import java.util.Comparator;

@FunctionalInterface
public interface Spectrum {
    public double at(double wavelength);

    public final Spectrum WHITE = w -> 1.0;
    public final Spectrum BLACK = w -> 0.0;
}

