package xyz.marsavic.gfxlab;

import javafx.util.Pair;

import java.util.Arrays;
import java.util.Comparator;

public class SplineSpectrum implements Spectrum {
    Pair<Double, Double>[] samples;
    double[] m;

    public SplineSpectrum(Pair<Double, Double>[] samples) {
        if (samples == null) {
            throw new NullPointerException();
        }
        this.samples = samples;
        int n = samples.length;

        double[] d = new double[n - 1];
        m = new double[n];


        for (int i = 1; i < samples.length; i++) {
            double h = samples[i].getKey() - samples[i - 1].getKey();
            if (h <= 0.0)
                throw new IllegalArgumentException("Samples must have strictly increasing x coordinates.");
            d[i - 1] = (samples[i].getValue() - samples[i - 1].getValue());
        }

        m[0] = d[0];
        for (int i = 1; i < n - 1; i++) {
            if (d[i] == 0.0) {
                m[i] = 0.0;
                m[i + 1] = 0.0;
            } else {
                double a = m[i] / d[i];
                double b = m[i + 1] / d[i];
                double h = a*a+b*b;
                if (h > 9.0) {
                    double t = 3.0 / h;
                    m[i] = t * a * d[i];
                    m[i + 1] = t * b * d[i];
                }
            }
        }
    }

    @Override
    public double at(double wavelength) {
        final int n = samples.length;

        if (wavelength <= samples[0].getKey()) {
            return  samples[0].getValue();
        }
        if (wavelength >= samples[n-1].getKey()) {
            return samples[n-1].getValue();
        }

        int i = Arrays.binarySearch(samples,
                new Pair<Double, Double>(Double.valueOf(wavelength), null),
                new DoublePairKeyComparator());
        if (i >= 0) {
            return samples[i].getValue();
        }
        i = -(i+2); // Invert negative result and get index of previous element
        double h = samples[i+1].getKey() - samples[i].getKey();
        double t = (wavelength - samples[i].getKey()) / h;
        return (samples[i].getValue() * (1 + 2*t) + h*m[i]*t) * (1 - t) * (1 - t)
                + (samples[i+1].getValue() * (3 - 2*t) + h * m[i+1] * (t - 1)) * t * t;
    }
}

class DoublePairKeyComparator implements Comparator<Pair<Double, Double>> {
    @Override
    public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
        double k1 = o1.getKey(), k2 = o2.getKey();
        if (k1 > k2)
            return 1;
        else if (k1 < k2) {
            return -1;
        } else {
            return 0;
        }
    }
}